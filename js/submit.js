    const scriptURL = 'https://script.google.com/macros/s/AKfycbxs-myJ9-J5pHmoLvhg8pbMWpBlJxTfHFfPJnp147YgaKNGI6w4/exec';
    const form = document.forms['rsvp-form'];

    form.addEventListener('submit', e => {
        e.preventDefault();
            fetch(scriptURL, {
                method: 'POST',
                body: new FormData(form)
            })
            .then((response) => { 
                clearForm();
                setUpToast();
                toastr.success('Thank you for confirming your attendance!');
            })
            .catch(error => {
                console.error('Error!', error.message);
                toastr.error('There was an error when submitting the form');
            });
    });

    function clearForm(){
        $("#email-input").val("");
        $("#phone-input").val("");
        $("#name-input").val("");
    }

    function setUpToast () {
        toastr.options.positionClass = "toast-top-center";
        toastr.options.closeButton = true;
        toastr.options.showMethod = 'slideDown';
        toastr.options.hideMethod = 'slideUp';
        toastr.options.progressBar = true;
      }