
    var countDownDate = new Date("Dec 5, 2020 15:00:00").getTime();
    // Update the count down every 1 second
    var x = setInterval(function () {

      // Get today's date and time
      var now = new Date().getTime();

      // Find the distance between now and the count down date
      var distance = countDownDate - now;

      // Time calculations for days, hours, minutes and seconds
      var days = Math.floor(distance / (1000 * 60 * 60 * 24));
      var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
      var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
      var seconds = Math.floor((distance % (1000 * 60)) / 1000);

      // Display the result in the element with id="countdown"
      document.getElementById("countdown").innerHTML = '<span style="color: #db4844;">' + days + "d " + "</span>" +
        '<span style="color: #f07c22;">' + hours + "h " + "</span>" +
        '<span style="color: #f6da74;">' + minutes + "m " + "</span>" +
        '<span style="color: #abcd58;">' + seconds + "s " + "</span>";
      //'<span style="color: blue;"> Until D-Day</span>';

      // If the count down is finished, write some text
      if (distance < 0) {
        clearInterval(x);
        document.getElementById("countdown").innerHTML = "EXPIRED";
      }
    }, 1000);
